package com.solvd.carina.demo.gui.swaglabs.pages;

import com.solvd.carina.demo.gui.swaglabs.pages.HomePage;
import com.zebrunner.carina.webdriver.decorator.ExtendedWebElement;
import com.zebrunner.carina.webdriver.decorator.PageOpeningStrategy;
import com.zebrunner.carina.webdriver.gui.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {
    @FindBy(id = "user-name")
    private ExtendedWebElement username;

    @FindBy(id = "password")
    private ExtendedWebElement password;

    @FindBy(id = "login-button")
    private ExtendedWebElement buttonLogIn;

    @FindBy(xpath = "//h3[@data-test='error']")
    private ExtendedWebElement errorWindow;

    public LoginPage(WebDriver driver) {
        super(driver);
        setPageAbsoluteURL("https://www.saucedemo.com/");
        setPageOpeningStrategy(PageOpeningStrategy.BY_ELEMENT);
        setUiLoadedMarker(username);
    }

    public HomePage login() {
        buttonLogIn.click();
        return new HomePage(driver);
    }

    public boolean isErrorWindowPresent () {
        return errorWindow.isElementPresent();
    }

    public String getErrorMessage () {
        return errorWindow.getText();
    }

    public void fillUsername (String userName) {
        username.type(userName);
    }

    public void fillPassword (String passWord) {
        password.type(passWord);
    }
}
