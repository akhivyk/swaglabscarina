package com.solvd.carina.demo.gui.swaglabs.components;

import com.solvd.carina.demo.gui.swaglabs.pages.HomePage;
import com.solvd.carina.demo.gui.swaglabs.pages.LoginPage;
import com.zebrunner.carina.webdriver.decorator.ExtendedWebElement;
import com.zebrunner.carina.webdriver.gui.AbstractUIObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class DropDownMenu extends AbstractUIObject {
    @FindBy(id = "inventory_sidebar_link")
    private ExtendedWebElement inventoryLink;

    @FindBy(id = "about_sidebar_link")
    private ExtendedWebElement aboutLink;

    @FindBy(id = "logout_sidebar_link")
    private ExtendedWebElement logoutLink;

    @FindBy(id = "reset_sidebar_link")
    private ExtendedWebElement resetLink;

    @FindBy(xpath = ".//button[contains(@id, 'react-burger-cross-btn')]")
    private ExtendedWebElement closeMenuButton;

    public DropDownMenu(WebDriver driver) {
        super(driver);
        setPageAbsoluteURL("https://www.saucedemo.com/inventory.html");
    }

    public void openLogoutPage () {
        logoutLink.click();
        new LoginPage(driver);
    }

    public void openAllItems () {
        inventoryLink.click();
    }

    public void openAboutPage () {
        aboutLink.click();
    }

    public void openResetLink () {
        resetLink.click();
    }

    public HomePage closeMenu () {
        closeMenuButton.click();
        return new HomePage(driver);
    }
}
