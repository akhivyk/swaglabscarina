package com.solvd.carina.demo;

import com.solvd.carina.demo.gui.swaglabs.components.DropDownMenu;
import com.solvd.carina.demo.gui.swaglabs.components.Item;
import com.solvd.carina.demo.gui.swaglabs.pages.*;
import com.zebrunner.agent.core.annotation.Maintainer;
import com.zebrunner.agent.core.annotation.TestCaseKey;
import com.zebrunner.agent.core.registrar.TestCase;
import com.zebrunner.carina.core.IAbstractTest;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SwagLabs implements IAbstractTest {

    @BeforeSuite
    public void setUp() {
        TestCase.setTestRunId("2772");
    }

    @Test
    @TestCaseKey("DEF-1084")
    @Maintainer("akhivyk")
    public void testLogin() {
        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.open();
        Assert.assertTrue(loginPage.isPageOpened(), "Login page is not opened.");

        loginPage.fillUsername("standard_user");
        loginPage.fillPassword("secret_sauce");

        HomePage homePage = loginPage.login();
        Assert.assertTrue(homePage.isPageOpened(), "Login failed.");
    }

    @Test
    @TestCaseKey("DEF-1086")
    @Maintainer("akhivyk")
    public void testLoginLogout() {
        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.open();
        Assert.assertTrue(loginPage.isPageOpened(), "Login page is not opened.");

        loginPage.fillUsername("standard_user");
        loginPage.fillPassword("secret_sauce");

        HomePage homePage = loginPage.login();
        Assert.assertTrue(homePage.isPageOpened(), "Login failed.");

        DropDownMenu dropDownMenu = homePage.openMenu();
        dropDownMenu.openLogoutPage();
        Assert.assertTrue(loginPage.isPageOpened(), "Failed logout.");
    }

    @Test
    @TestCaseKey("DEF-1089")
    @Maintainer("akhivyk")
    public void testItemInfo() {
        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.open();
        Assert.assertTrue(loginPage.isPageOpened(), "Login page is not opened.");

        loginPage.fillUsername("standard_user");
        loginPage.fillPassword("secret_sauce");
        HomePage homePage = loginPage.login();

        Item item = homePage.selectItem("Sauce Labs Backpack");
        String itemName = item.getItemName();
        String itemInfo = item.getItemInfo();
        String itemPrice = item.getItemPrice();

        ItemPage itemPage = item.openItemPage();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(itemPage.getItemName(), itemName, "Invalid item name.");
        softAssert.assertEquals(itemPage.getItemInfo(), itemInfo, "Invalid item description.");
        softAssert.assertEquals(itemPage.getItemPrice(), itemPrice, "Invalid item price.");
        softAssert.assertAll();
    }

    @Test
    @TestCaseKey("DEF-1084")
    @Maintainer("akhivyk")
    public void testLoginWithAnotherCred() {
        LoginPage loginPage = new LoginPage(getDriver());

        loginPage.open();
        Assert.assertTrue(loginPage.isPageOpened(), "Login page is not opened.");

        loginPage.fillUsername("standard_user");
        loginPage.fillPassword("secret_sauce_solvd");

        HomePage homePage = loginPage.login();
        Assert.assertTrue(homePage.isPageOpened(), "Login failed.");
    }
}
